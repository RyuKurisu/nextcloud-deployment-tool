#!/bin/bash

backtitle="Nextcloud.com - Nextcloud deployment tool."

function generalSetUp()
{
	# update all packages	
	apt update && apt upgrade -y

	# check for specific apps and install them if needed
	type -P git &>/dev/null && echo "Found git command." || { echo "apt install -y git"; }
	type -P dialog &>/dev/null && echo "Found dialog command." || { echo "apt install -y dialog"; }
	
	# enable US UTF-8 locale
	locale-gen en_US.UTF-8
	sudo sed -i -e "s/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g" /etc/locale.gen
	
	# make sure that the group www-data exists
	groupadd www-data
	usermod -a -G www-data www-data
	
	# set ARM frequency to 800 MHz (attention: should be safe, but do this at your own risk)
	ensureKeyValueShort "arm_freq" "800" "/boot/config.txt"
	ensureKeyValueShort "sdram_freq" "450" "/boot/config.txt"
	ensureKeyValueShort "core_freq" "350" "/boot/config.txt"

	# resize swap file to 512 MB
	ensureKeyValueShort "CONF_SWAPSIZE" "512" "/etc/dphys-swapfile"
	dphys-swapfile setup
	dphys-swapfile swapon
	
	# change RAM settings 16 MB video RAM
	ensureKeyValueShort "gpu_mem" "16" "/boot/config.txt"
}

# arg 1: key, arg 2: value, arg 3: file
function ensureKeyValue()
{
    if [[ -z $(egrep -i ";? *$1 = [0-9]*[M]?" $3) ]]; then
        # add key-value pair
        echo "$1 = $2" >> $3
    else
        # replace existing key-value pair
        toreplace=`egrep -i ";? *$1 = [0-9]*[M]?" $3`
        sed $3 -i -e "s|$toreplace|$1 = $2|g"
    fi     
}

# arg 1: key, arg 2: value, arg 3: file
# make sure that a key-value pair is set in file
# key=value
function ensureKeyValueShort()
{
    if [[ -z $(egrep -i "#? *$1\s?=\s?""?[+|-]?[0-9]*[a-z]*"""? $3) ]]; then
        # add key-value pair
        echo "$1=""$2""" >> $3
    else
        # replace existing key-value pair
        toreplace=`egrep -i "#? *$1\s?=\s?""?[+|-]?[0-9]*[a-z]*"""? $3`
        sed $3 -i -e "s|$toreplace|$1=""$2""|g"
    fi     
}

function downloadLatestNextcloudRelease()
{
	clear

	if [[ ! -d /var/www/nextcloud ]]; then
		mkdir -p /var/www/nextcloud
	fi

	dialog --backtitle "$backtitle" --msgbox "Updating to latest Nextcloud release." 20 60

	# download and extract the latest release of Nextcloud
	version=$(wget -q -O - https://github.com/nextcloud/server/releases/latest | grep -o 'Release.*[0-9]\+.' | sed -e 's/Release v//' -e 's/ //')
	latestrelease="https://download.nextcloud.com/server/releases/nextcloud-$version.tar.bz2"
	wget "$latestrelease"
	wget "$latestrelease.sha256"
	wget "$latestrelease.asc"
	wget "https://nextcloud.com/nextcloud.asc"
	gpg --import nextcloud.asc
	gpg --verify nextcloud-$version.tar.bz2.asc nextcloud-$version.tar.bz2
	if [[ $(sha256sum -c nextcloud-$version.tar.bz2.sha256 < nextcloud-$version.tar.bz2) = "nextcloud-$version.tar.bz2: OK" ]]; then
		# Extract tarball
		tar -xjf nextcloud-$version.tar.bz2 -C /var/www/
		chown -R www-data:www-data /var/www
	else
		exit 1
	fi
	# Remove checkfiles
	rm nextcloud*
}

function writeServerConfig()
{
	cat > /etc/nginx/sites-available/default << _EOF_
# nextcloud
server {
	listen 80;
	server_name $__servername;
	return 301 https://$host$request_uri;  # enforce https
}

# nextcloud (ssl/tls)
server {
	listen 443 ssl;
	server_name $__servername;
	ssl_certificate /etc/letsencrypt/live/$__servername/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/$__servername/privkey.pem;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_prefer_server_ciphers on;
	ssl_dhparam /etc/ssl/certs/dhparam.pem;
	ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:ECDHE-RSA-AES128-GCM-SHA256:AES256+EECDH:DHE-RSA-AES128-GCM-SHA256:AES256+EDH:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4"; 
	ssl_session_timeout 1d;
	ssl_session_cache shared:SSL:50m;
	ssl_stapling on; 
	ssl_stapling_verify on; 
	add_header Strict-Transport-Security max-age=15768000;

	root /var/www;
	index index.php;
	client_max_body_size 4000M; # set maximum upload size
	fastcgi_buffers 64 4K;

	# deny direct access
	location ~ ^/nextcloud/(data|config|\.ht|db_structure\.xml|README) {
    	deny all;
	}

	# default try order
	location / {
		try_files \$uri \$uri/ index.php;
	}

	# allow all to Let's Encrypt folder
	location ~ /.well-known { 
		allow all; 
	}

	# nextcloud WebDAV
	location @webdav {
		fastcgi_split_path_info ^(.+\.php)(/.*)$;
		fastcgi_pass 127.0.0.1:9000;
		fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
		fastcgi_param HTTPS on;
		include fastcgi_params;
	}

	# enable php
	location ~ ^(?<script_name>.+?\.php)(?<path_info>/.*)?$ {
		try_files \$script_name = 404;
		include fastcgi_params;
		fastcgi_param PATH_INFO \$path_info;
		fastcgi_param HTTPS on;
		fastcgi_pass 127.0.0.1:9000;
		fastcgi_read_timeout 900s; # 15 minutes
	}
}    
_EOF_
}

function main_setservername()
{
	cmd=(dialog --backtitle "$backtitle" --inputbox "Please enter the URL of your Nextcloud server." 22 76 $__servername)
	choices=$("${cmd[@]}" 2>&1 >/dev/tty)    
	if [ "$choices" != "" ]; then
		__servername=$choices
	fi
	if [[ -f /etc/nginx/sites-available/default ]]; then
		sed /etc/nginx/sites-available/default -r -e "s|server_name .*|server_name $__servername;|g"
	fi
	else
		break
	fi
}

function installCertificateNginx()
{
	dialog --backtitle "$backtitle" --msgbox "We are now going to create a self-signed certificate. While you could simply press ENTER when you are asked for country name etc. or enter whatever you want, it might be beneficial to have the web servers host name in the common name field of the certificate." 20 60
	clear
	openssl req $@ -new -x509 -days 365 -nodes -out /etc/nginx/cert.pem -keyout /etc/nginx/cert.key
	chmod 600 /etc/nginx/cert.pem
	chmod 600 /etc/nginx/cert.key
}

function main_newinstall_nginx()
{
	clear
	
	# install all needed packages, e.g., Nginx, PHP, SQLite
	apt install -y nginx sendmail sendmail-bin openssl ssl-cert php5-cli php5-sqlite php5-gd php5-curl \
                      php5-common php5-cgi sqlite php-pear php-apc git-core \
                      autoconf automake autotools-dev libapr1 libtool curl libcurl4-openssl-dev \
                      php-xml-parser php5 php5-dev php5-gd php5-fpm memcached php5-memcache varnish dphys-swapfile bzip2
	apt autoremove -y

	# generate self-signed certificate that is valid for one year
	installCertificateNginx
	
	writeServerConfig
	sed /etc/php5/fpm/pool.d/www.conf -i -e "s|listen = /var/run/php5-fpm.sock|listen = 127.0.0.1:9000|g"

	ensureKeyValue "upload_max_filesize" "1000M" "/etc/php5/fpm/php.ini"
	ensureKeyValue "post_max_size" "1000M" "/etc/php5/fpm/php.ini"
	ensureKeyValue "default_charset" "UTF-8" "/etc/php5/fpm/php.ini"

	ensureKeyValue "upload_tmp_dir" "/srv/http/nextcloud/data" "/etc/php5/fpm/php.ini"
	mkdir -p /srv/http/nextcloud/data
	chown www-data:www-data /srv/http/nextcloud/data

	sed /etc/nginx/sites-available/default -i -e "s|client_max_body_size [0-9]*[M]?;|client_max_body_size 1000M;|g"

	/etc/init.d/php5-fpm restart
	/etc/init.d/nginx restart

	downloadLatestNextcloudRelease

	# finish the script
	myipaddress=$(hostname -I | tr -d ' ')
	dialog --backtitle "$backtitle" --msgbox "If everything went right, Nextcloud should now be available at the URL https://$myipaddress/nextcloud. You have to finish the setup by visiting that site." 20 60    
}

function installCertificateApache()
{  
	dialog --backtitle "$backtitle" --msgbox "We are now going to create a self-signed certificate. While you could simply press ENTER when you are asked for country name etc. or enter whatever you want, it might be beneficial to have the web servers host name in the common name field of the certificate." 20 60    
	clear
	openssl req $@ -new -x509 -days 365 -nodes -out /etc/apache2/apache.pem -keyout /etc/apache2/apache.key
	chmod 600 /etc/apache2/apache.pem
	chmod 600 /etc/apache2/apache.key
}

function main_newinstall_apache()
{
	clear 

	# install all needed packages, e.g., Apache, PHP, SQLite
	apt install -y apache2 openssl sendmail sendmail-bin ssl-cert libapache2-mod-php5 php5-cli php5-sqlite php5-gd php5-curl php5-common php5-cgi sqlite php-pear php-apc git-core ca-certificates dphys-swapfile bzip2

	# generate self-signed certificate that is valid for one year
	installCertificateApache

	# enable Apache modules (as explained at http://nextcloud.org/support/install/, Section 2.3)
	a2enmod ssl
	a2enmod rewrite
	a2enmod headers

	# disable unneccessary (for Nextcloud) module(s)
	a2dismod cgi
	a2dismod authz_groupfile

	# configure Apache to use self-signed certificate
	mv /etc/apache2/sites-available/default-ssl /etc/apache2/sites-available/default-ssl.bak
	sed 's|/etc/ssl/certs/ssl-cert-snakeoil.pem|/etc/apache2/apache.pem|g' /etc/apache2/sites-available/default-ssl.bak > tmp
	sed -i 's|AllowOverride None|AllowOverride All|g' tmp
	sed -i 's|/etc/ssl/private/ssl-cert-snakeoil.key|/etc/apache2/apache.key|g' tmp
	mv tmp /etc/apache2/sites-available/default-ssl

	# limit number of parallel Apache processes
	mv /etc/apache2/apache2.conf /etc/apache2/apache2.conf.bak 
	sed 's|StartServers          5|StartServers          2|g;s|MinSpareServers       5|MinSpareServers       2|g;s|MaxSpareServers      10|MaxSpareServers       3|g' /etc/apache2/apache2.conf.bak > tmp
	mv tmp /etc/apache2/apache2.conf

	# enable SSL site
	a2ensite default-ssl

	downloadLatestNextcloudRelease

	# restart Apache service
	/etc/init.d/apache2 reload

	# finish the script
	myipaddress=$(hostname -I | tr -d ' ')
	dialog --backtitle "$backtitle" --msgbox "If everything went right, Nextcloud should now be available at the URL https://$myipaddress/nextcloud. You have to finish the setup by visiting that site. Before that, we are going to reboot the Raspberry." 20 60    
	reboot
}

function main_update()
{
	downloadLatestNextcloudRelease
	dialog --backtitle "$backtitle" --msgbox "Finished upgrading Nextcloud instance." 20 60    
}

function main_updatescript()
{
	scriptdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	pushd $scriptdir
	if [[ ! -d .git ]]; then
		dialog --backtitle "$backtitle" --msgbox "Cannot find direcotry '.git'. Please clone the NextcloudPie script via 'git clone git://github.com/petrockblog/NextcloudPie.git'" 20 60    
		popd
		return
	fi
	git pull
	popd
	dialog --backtitle "$backtitle" --msgbox "Fetched the latest version of the NextcloudPie script. You need to restart the script." 20 60
}

function main_uninstall()
{
	dialog --backtitle "$backtitle" --title "Uninstall NextcloudPie" --clear --defaultno --yesno "Do you really want to UNINSTALL NextcloudPie?" 20 60

	case $? in
		0)
		apt autoremove -y nginx sendmail sendmail-bin openssl ssl-cert php5-cli php5-sqlite php5-gd php5-curl \
		apache2 php5-common php5-cgi sqlite php-pear php-apc git-core \
		autoconf automake autotools-dev curl libapr1 libtool curl libcurl4-openssl-dev \
		php-xml-parser php5 php5-dev php5-gd php5-fpm memcached php5-memcache varnish dphys-swapfile bzip2
	;;
		*)
	;;
	esac      
}

# here starts the main script

if [ $(id -u) -ne 0 ]; then
	printf "Script must be run as root. Try 'sudo ./nextcloudpie.sh'\n"
	exit 1
fi

generalSetUp

if [[ -f /etc/nginx/sites-available/default ]]; then
	__servername=$(egrep -m 1 "server_name " /etc/nginx/sites-available/default | sed "s| ||g")
	__servername=${__servername:11:0-1}
else
	__servername="url.ofmyserver.com"
fi

while true; do
	cmd=(dialog --backtitle "$backtitle" --menu "You MUST set the server URL (e.g., 192.168.0.10 or myaddress.dyndns.org) before starting one of the installation routines. Choose task:" 22 76 16)
	options=(1 "Set server URL ($__servername)"
		2 "New NGiNX based installation"
		3 "Generate new SSL certificate for NGiNX"
		4 "New Apache based installation"
		5 "Generate new SSL certificate for Apache"
		6 "Update existing Nextcloud installation"
		7 "Update NextcloudPie script"
		8 "Uninstall NextcloudPie")
	choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)    
	if [ "$choice" != "" ]; then
	case $choice in
		1) main_setservername ;;
		2) main_newinstall_nginx ;;
		3) installCertificateNginx ;;
		4) main_newinstall_apache ;;
		5) installCertificateApache ;;
		6) main_update ;;
		7) main_updatescript ;;
		8) main_uninstall ;;
	esac
	else
		break
	fi
done
clear
